{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";

    sops-nix.url = "github:Mic92/sops-nix";
    nur.url = "github:nix-community/NUR";
    nixvim.url = "github:nix-community/nixvim";
    
    stylix.url = "github:danth/stylix";
    simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-24.05";
  };

  outputs =
    { nixpkgs
    , home-manager
    , sops-nix
    , nixvim
    , nur
    , stylix
    , simple-nixos-mailserver
    , ...
    } @ inputs: {

      # LAPTOP 1
      nixosConfigurations = {
        march = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs; };
          modules = [
            ./users/march/system.nix
            ./users/shared
	    nur.modules.nixos.default
            simple-nixos-mailserver.nixosModule
            sops-nix.nixosModules.sops
	    stylix.nixosModules.stylix
            home-manager.nixosModules.home-manager
            {
              nix.registry.nixpkgs.flake = nixpkgs;
              nix.nixPath = [ "nixpkgs=flake:nixpkgs" ];

              home-manager = {
                users.aibarchok = ./users/march/home.nix;
                extraSpecialArgs = { inherit inputs; };
                useGlobalPkgs = true;
                useUserPackages = true;
                sharedModules = [
		  nur.modules.homeManager.default
                  inputs.sops-nix.homeManagerModules.sops
                  inputs.nixvim.homeManagerModules.nixvim
                ];
              };
            }
          ];
        };
      };

      # LAPTOP 2 (consider it as a desktop) :D
      nixosConfigurations = {
        april = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs; };
          modules = [
            ./users/april/system.nix
            ./users/shared
	    nur.modules.nixos.default
            simple-nixos-mailserver.nixosModule
            sops-nix.nixosModules.sops
            stylix.nixosModules.stylix
            home-manager.nixosModules.home-manager
            {
              nix.registry.nixpkgs.flake = nixpkgs;
              nix.nixPath = [ "nixpkgs=flake:nixpkgs" ];

              home-manager = {
                users.aibarchok = ./users/april/home.nix;
                extraSpecialArgs = { inherit inputs; };
                useGlobalPkgs = true;
                useUserPackages = true;
                sharedModules = [
		  nur.modules.homeManager.default
                  inputs.sops-nix.homeManagerModules.sops
                  inputs.nixvim.homeManagerModules.nixvim
                ];
              };
            }
          ];
        };
      };

      # SERVER
      nixosConfigurations = {
        may = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs; };
          modules = [
            ./users/may/system.nix
            ./users/shared/minimal.nix
            {
              nix.registry.nixpkgs.flake = nixpkgs;
              nix.nixPath = [ "nixpkgs=flake:nixpkgs" ];
            }
            sops-nix.nixosModules.sops
            simple-nixos-mailserver.nixosModule
          ];
        };
      };
    };
}
