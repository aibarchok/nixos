{ lib, config, ... }: {
  options.modules.bat.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.bat.enable {
    programs.bat = {
      enable = true;
      config = {
        pager = "less -FR";
        #theme = "Dracula";
      };
    };
  };
}
