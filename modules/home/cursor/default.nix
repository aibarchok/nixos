{ lib, config, pkgs, ... }: {
  options.modules.cursor.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.cursor.enable {
    home.pointerCursor = lib.mkForce {
      gtk.enable = true;
      x11.enable = true;
      name = "hackneyed-cursors";
      package = pkgs.hackneyed;
      size = 28;
    };
  };
}
