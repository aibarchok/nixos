{ ... }: {
  imports = [
    #./alacritty
    ./bat
    ./cursor
    ./dunst
    #./hyprland
    ./git
    ./firefox
    ./vscode
    #./waybar
    ./rofi
    #./wofi
    ./mako
    ./lf
    ./picom
    ./neovim
    ./ncmpcpp
    #./starship
    #./swaylock
    ./xdg
    #./zoxide
    ./zsh
  ];
}
