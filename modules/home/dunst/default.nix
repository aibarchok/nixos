{ lib, config, ... }: {
  options.modules.dunst.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.dunst.enable {
    services.dunst = {
      enable = true;
      settings.global = {
        #font = "Monospace 10";
        #font = "DejaVu Sans 11";
        #frame_color = "#C5C8C6";
      };
    };
  };
}
