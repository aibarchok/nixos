{ lib, config, ... }: {
  options.modules.git.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.git.enable {
    programs.git = {
      enable = true;
      userName = "aibarchok";
      #userName = config.sops.secrets."github-user-name".path;
      userEmail = "aibarchok@protonmail.com";
      #userEmail = config.sops.secrets."github-user-email".path;
    };
  };
}
