{ lib, ... }: {

  options.modules.hyprland.enable = lib.mkEnableOption "";

  imports = [
    ./hyprland.nix
    ./bindings.nix
    #./variables.nix
  ];
}

    
