{ inputs, pkgs, ... }: {
  home.packages = with pkgs; [
    swaybg
    hyprpicker
    hyprcursor
    grimblast
    grim
    slurp
    brightnessctl
    wl-clip-persist
    wf-recorder
    wl-clipboard
    glib
    wayland
    direnv
  ];

  systemd.user.targets.hyprland-session.Unit.Wants = [ "xdg-desktop-autostart.target" ];
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    plugins = [
      # inputs.hypr-contrib.packages.${pkgs.system}.grimblast
      # ...
    ];
    systemd.enable = true;
  };
}

