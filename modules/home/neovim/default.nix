{ lib, config, ... }: {
  options.modules.neovim.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.neovim.enable {
    programs.nixvim = {
      enable = true;

      opts = {
        shiftwidth = 2;
        relativenumber = false;
        number = true;
        updatetime = 100;
      };

      plugins = {
        lightline = {
          enable = true;
        };

        nvim-tree = {
          enable = true;
          autoReloadOnWrite = true;
        };

        auto-save = {
          enable = false;
          settings.enabled = true;
        };

        lsp = {
          enable = true;
          servers = {
            nixd.enable = true;
          };
        };
        web-devicons = {
          enable = true;
        };
      };
    };
    #colorschemes = {
    #modus.enable = true;
    #};
  };
}
