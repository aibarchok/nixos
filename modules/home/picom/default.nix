{ lib, config, ... }: {
  options.modules.picom.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.picom.enable {
    services.picom = {
      enable = true;
      vSync = true;
      backend = "glx";
      shadow = false;
      shadowExclude = [
        "window_type = 'dock'"
      ];
    };
  };
}
