{ lib, config, ... }: {
  options.modules.rofi.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.rofi.enable {
    programs.rofi = {
      enable = true;
      extraConfig = "";
      cycle = true;
      location = "bottom";
      #terminal = "{pkgs.st";

    };
  };
}
