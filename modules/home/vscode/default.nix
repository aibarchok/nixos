{ lib, config, pkgs, ... }: {

  options.modules.vscode.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.vscode.enable {
    programs.vscode = {
      enable = true;
      enableUpdateCheck = false;
      enableExtensionUpdateCheck = false;
      package = pkgs.vscodium-fhs;
      extensions = with pkgs.vscode-extensions; [
        # UI Theme
        catppuccin.catppuccin-vsc

        # Icon Theme
        catppuccin.catppuccin-vsc-icons

        # Nix
        jnoortheen.nix-ide

        # Go
        golang.go

        # Hugo
        #budparr.language-hugo-vscode

        # Pandoc
        #garlicbreadcleric.pandoc-markdown-syntax

        # Other
        naumovs.color-highlight
        mkhl.direnv
      ];

      userSettings = {
        "window.menuBarVisibility" = "toggle";
        "workbench.startupEditor" = "none";
        "workbench.iconTheme" = "catppuccin-macchiato";
        #"workbench.colorTheme" = "Catppuccin Mocha";
        "files.autoSave" = "afterDelay";
        "git.autofetch" = true;
        "git.enableSmartCommit" = true;
        "git.confirmSync" = false;
        "editor.cursorSmoothCaretAnimation" = "on";
        "editor.tabSize" = 1;
        "editor.detectIndentation" = false;
        #"editor.fontFamily" = "'FiraCode Nerd Font'";
        "workbench.statusBar.visible" = false;
        "editor.minimap.enabled" = false;
        "breadcrumbs.enabled" = false;
        "window.zoomLevel" = 1;
        "editor.codeLens" = false;
      };
    };
  };
}
