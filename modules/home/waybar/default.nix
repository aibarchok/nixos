{ lib, ... }: {

  options.modules.waybar.enable = lib.mkEnableOption "";

  imports = [
    ./waybar.nix
    ./settings.nix
    ./style.nix
  ];
}

