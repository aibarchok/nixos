{ lib, config, ... }: {
  options.modules.zsh.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.zsh.enable {
    programs.zsh = {
      enable = true;
      oh-my-zsh = {
        enable = true;
        plugins = [
          "arduino-cli"
          "dotenv"
          "direnv"
          "zsh-interactive-cd"
          "zsh-navigation-tools"
        ];
        theme = "dieter";
      };
      enableCompletion = true;
      dotDir = ".config/zsh";
      history = {
        path = "${config.xdg.dataHome}/zsh/zsh_history";
        expireDuplicatesFirst = true;
        ignoreSpace = false;
        save = 15000;
        share = true;
      };

      shellAliases = rec {
        nix-check = "alejandra -q **/* && deadnix -e && statix fix";
        nix-rebuild-git = "doas nixos-rebuild switch --flake gitlab:aibarchok/nixos#$(hostname) --refresh --option eval-cache false";
        nix-rebuild = "doas nixos-rebuild switch --flake .#$(hostname)";
        nix-clean = "doas nix-collect-garbage -d";
        nix-update = "doas nix-channel --update";
	
	steam = "nvidia-offload flatpak run com.valvesoftware.Steam";

        l = "lsd -l";
        ls = "lsd";
        v = "nvim";
        c = "clear";
        cp = "cp -iv";
        bc = "bc -ql";
        ka = "killall";
        yt = "yt-dlp --embed-metadata -i";
        yta = "yt -x -f bestaudio/best";
        dev-shell = "nix-shell /etc/nixos/shell.nix";
        ffmpeg = "ffmpeg -hide_banner";
        wget = "wget --no-hsts";
        gcl = "git clone --depth=1 --filter=blob:none";
        sxiv = "nsxiv";
        cdtmp = "cd $(mktemp -d)";

        ".." = "cd ..";
        "..." = "cd ...";
      };
    };
  };
}
    
