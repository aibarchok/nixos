{ ... }: {
  imports = [
    ./kernel
    ./networkmanager
    ./nvidia
    ./radeon
    ./pipewire
    ./systemd-boot
    ./bluetooth
    ./steam
    ./stylix
    ./lightdm
    ./mpd
    ./tlp
    ./avahi
    #./wayland
    ./xorg
    ./suckless
    ./virt
    ./lact
    ./flatpak
  ];
}
