{ lib, config, pkgs, ... }: {
  options.modules.flatpak.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.flatpak.enable {
  xdg.portal = {
  enable = true;
  extraPortals = [ pkgs.pkgs.xdg-desktop-portal-gtk ]; 
  config.common.default = "*";
  };
  services.flatpak.enable = true; 
    };
  }
