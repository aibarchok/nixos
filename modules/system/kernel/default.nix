{ lib, config, pkgs, ... }: {

  options.modules.kernel.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.kernel.enable {
    boot = {
      blacklistedKernelModules = [
        "pcspkr"
        "nouveau"
      ];
      kernelParams = [
        "fbcon=nodefer"
        "bgrt_disable"
        "quiet"
        "rd.systemd.show_status=false"
        "rd.udev.log_level=0"
        "udev.log_priority=3"
        "vt.global_cursor_default=0"
        "i915"
        "nvidia_uvm"
        "nvidia_modeset"
        "nvidia"
        "nvidia_drm.fbdev=1"
        "nvidia-drm.modeset=1" # required by gamescope
        "mitigations=off"
        "kernel.nmi_watchdog=0"
        #"vsyscall=emulate"
        #"clearcpuid=304" # disable avx-512
        #"clearcpuid=514" # disable umip (sgdt)
      ];
      consoleLogLevel = 0;
      initrd.verbose = false;
      supportedFilesystems = [ "ntfs" ];
      kernelPackages = pkgs.linuxPackages_zen;
    };
  };
}
