{ lib, config, ... }: {

  options.modules.lightdm.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.lightdm.enable {
    security.polkit.enable = true;

    services.xserver.displayManager = {
      lightdm = {
        enable = true;
        greeters.gtk.enable = true;
        extraConfig = ''
          logind-check-graphical = true '';
      };
    };
  };
}

