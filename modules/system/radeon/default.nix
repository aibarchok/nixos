{ lib, config, ... }: {

  options.modules.radeon.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.radeon.enable {
    services.xserver.videoDrivers = [ "amdgpu" ];
  };
}

