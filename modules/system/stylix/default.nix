{ lib, config, pkgs, ... }:

let
  opacity = 0.95;
  fontSize = 11;
in
{

  options.modules.stylix.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.stylix.enable {
    stylix = {
      enable = true;
      image = ./wallpaper.png;
      polarity = "dark";

      targets = {
        gtk.enable = true;
      };

      opacity = {
        terminal = opacity;
        popups = opacity;
      };

      cursor = {
        package = pkgs.hackneyed;
        name = "hackneyed-cursors";
        size = 28;
      };

      fonts = {
        serif = {
          package = pkgs.poly;
          name = "Poly";
        };

        sansSerif = {
          package = pkgs.fira-sans;
          name = "Fira Sans";
        };

        monospace = {
          package = pkgs.fira-mono;
          name = "Fira Mono";
        };


        emoji = {
          package = pkgs.noto-fonts-emoji;
          name = "Noto Color Emoji";
        };

        sizes = {
          applications = fontSize;
          desktop = fontSize;
          popups = fontSize;
          terminal = fontSize;
        };
      };
    };
  };
}

