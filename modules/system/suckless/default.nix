{ inputs, lib, config, pkgs, ... }: {
  options.modules.suckless.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.suckless.enable {
    environment.systemPackages = with pkgs; [
      dwm
      st
      dmenu
      slstatus

      flameshot
      light
  ];

    services.displayManager.defaultSession = "none+dwm";

    programs.light.enable = true;

    nixpkgs.overlays = [
      (final: prev: {
        dwm = prev.dwm.overrideAttrs (old: {
          src = ../../../overlays/dwm;
          buildInputs = [ pkgs.ix pkgs.xorg.libXinerama pkgs.xorg.xorgproto pkgs.xorg.libX11 pkgs.xorg.libXft ];
          patches = [
            (pkgs.fetchpatch {
              url = "https://dwm.suckless.org/patches/autostart/dwm-autostart-20210120-cb3f58a.diff";
              sha256 = "sha256-mrHh4o9KBZDp2ReSeKodWkCz5ahCLuE6Al3NR2r2OJg=";
            })
	    
	    (pkgs.fetchpatch {
              url = "https://dwm.suckless.org/patches/backlight/dwm-backlight-20241021-351084d.diff";
              sha256 = "sha256-ibUkz0M2bp5aYz0xLpwLNOBjyzb0WEkFBU8LPp/bdKU=";
            })
          ];
        });

        st = prev.st.overrideAttrs (old: {
          src = ../../../overlays/st;
          buildInputs = [ pkgs.harfbuzz pkgs.freetype pkgs.fontconfig pkgs.xorg.xorgproto pkgs.xorg.libX11 pkgs.xorg.libXft ];
        });

        dmenu = prev.dmenu.overrideAttrs (old: {
          src = ../../../overlays/dmenu;
          buildInputs = [ pkgs.xorg.libXinerama pkgs.xorg.xorgproto pkgs.xorg.libX11 pkgs.xorg.libXft ];
        });

        slstatus = prev.slstatus.overrideAttrs (old: {
          src = ../../../overlays/slstatus;
          buildInputs = [ pkgs.xorg.libXau pkgs.xorg.libXdmcp pkgs.xorg.xorgproto pkgs.xorg.libX11 pkgs.xorg.libXft ];
	   patches = [
            (pkgs.fetchpatch {
              url = "https://tools.suckless.org/slstatus/patches/alsa-master/slstatus-alsa-master-20230420-84a2f11.diff";
              sha256 = "1wq40garahp19j5aqj2j5qza9v7a290mf5v4pc50cc4vwv28nawj";
            })
          ];
        });
      })
    ];
  };
}
