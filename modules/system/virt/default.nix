{ lib, config, pkgs, ... }: {
  options.modules.virt.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.virt.enable {
    users.users.aibarchok.extraGroups = [ "libvirtd" ];
    programs.virt-manager.enable = true;
    services.qemuGuest.enable = true;

    environment.systemPackages = with pkgs; [
      libvirt-glib
      virt-manager
      virt-viewer
      spice
      spice-gtk
      spice-protocol
      win-virtio
      win-spice
      adwaita-icon-theme
    ];

    virtualisation = {
      libvirtd = {
        enable = true;
        qemu = {
          swtpm.enable = true;
          ovmf.enable = true;
          ovmf.packages = [ pkgs.OVMFFull.fd ];
        };
      };
      spiceUSBRedirection.enable = true;
    };
    services.spice-vdagentd.enable = true;
  };
}
