{ lib, config, pkgs, ... }: {
  options.modules.wayland.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.wayland.enable {
    programs.hyprland.enable = true;
    xdg.portal = {
      enable = true;
      wlr.enable = true;
      xdgOpenUsePortal = true;
      extraPortals = [
        pkgs.xdg-desktop-portal-hyprland
        pkgs.xdg-desktop-portal-gtk
      ];
    };
  };
}
