{ lib, config, pkgs, ... }: {
  options.modules.xorg.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.xorg.enable {
    services = {
      xserver = {
        enable = true;
        windowManager.dwm.enable = true;
        xkb = {
          layout = "us,ru";
          variant = "workman";
          options = "grp:caps_toggle";
        };
      };

      libinput = {
        enable = true;

        mouse = {
          accelProfile = "flat";
        };

        touchpad = {
          accelProfile = "flat";
        };
      };
    };
    environment.systemPackages = with pkgs; [
      xorg.xbacklight
      shutter
    ];

    services.xserver.excludePackages = with pkgs; [
      xterm
    ];

    systemd.extraConfig = "DefaultTimeoutStopSec=10s";
  };
}
