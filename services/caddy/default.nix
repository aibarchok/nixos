{ config, lib, ... }: {
  options.modules.services.caddy.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.services.caddy.enable {
    services.caddy = {
      enable = true;
      globalConfig = ''
        debug
      '';
      virtualHosts."jennbob.xyz" = {
        useACMEHost = "jennbob.xyz";
        extraConfig = '' 
        root * /var/www/html/webpage/public
        file_server
	
	handle_errors {
    	rewrite * /404/index.html
    	file_server
      	}
	'';
      };
      virtualHosts."forgejo.jennbob.xyz" = {
        extraConfig = '' reverse_proxy http://127.0.0.1:3000 '';
      };
    };
    networking.firewall.allowedTCPPorts = [ 80 443 3000 ];
  };
}

