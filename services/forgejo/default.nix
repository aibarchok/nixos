{ config, lib, ... }:

options.modules.services.forgejo.enable = lib.mkEnableOption "";

config = lib.mkIf config.modules.services.forgejo.enable {
services.forgejo = {
enable = true;
stateDir = "/srv/forgejo";
user = "git";
group = "git";
settings = {
DEFAULT = {
APP_NAME = "jennbob.xyz git Server";
};
server = {
HTTP_PORT = 3001;
DOMAIN = "git.jennbob.xyz";
ROOT_URL = "https://git.jennbob.xyz";
};
service = {
#REGISTER_EMAIL_CONFIRM = true;
REGISTER_MANUAL_CONFIRM = true;
};
mailer = { # Password is plain!
ENABLED = true;
PROTOCOL = "smtps";
SMTP_ADDR = "jennbob.xyz";
USER = "forgejo@jennbob.xyz";
FROM = "forgejo@jennbob.xyz";
PASSWD = "${builtins.readFile /UR/DIR/HERE}";
};
openid = {
ENABLE_OPENID_SIGNIN = true;
ENABLE_OPENID_SIGNUP = true;
};
"cron.delete_inactive_accounts" = {
ENABLED = true;
SCHEDULE = "@midnight";
};
};
};
};
}
