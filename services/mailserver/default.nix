{ lib, config, ... }: {
  options.modules.services.mailserver.enable = lib.mkEnableOption "";

  config = lib.mkIf config.modules.services.mailserver.enable {
    mailserver = {
      enable = true;
      fqdn = "jennbob.xyz";
      domains = [ "jennbob.xyz" ];

      # IMAP (143)
      enableImap = true;

      # SMTP (587)
      enableSubmission = true;

      localDnsResolver = false;
      openFirewall = true;
      loginAccounts = {
        "me@jennbob.xyz" = {
          hashedPasswordFile = "/var/www/mail/password";
        };
      };
      certificateScheme = "acme";
    };

    security.acme = {
      acceptTerms = true;
      defaults.email = "acme@jennbob.xyz";
      defaults.dnsProvider = "porkbun";
      defaults.environmentFile = "/var/www/dns/tsig-secret-jennbob.xyz";
    };
  };
}
