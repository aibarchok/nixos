{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  packages = with pkgs; [
    statix
    alejandra
    nixpkgs-fmt
    deadnix

    sops
  ];
}
