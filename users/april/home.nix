{ pkgs, ... }:

{
  imports = [
    ../../modules/home
  ];

  home.packages = with pkgs; [
    keepassxc
    thunderbird

    transmission_4-qt6
    nicotine-plus

    obs-studio

    telegram-desktop
    gajim
    vesktop

    tor-browser

    krita
    nsxiv
    gimp

    libreoffice-qt
    hunspell
    hunspellDicts.en_US
    hunspellDicts.ru_RU

    networkmanagerapplet
    librewolf
  ];

  modules = {
    # de-env
    firefox.enable = true;
    xdg.enable = true;

    # wm-env
    dunst.enable = true;
    picom.enable = true;

    # dev-env
    vscode.enable = true;
    git.enable = true;
    neovim.enable = true;

    # tui-env
    lf.enable = true;
    ncmpcpp.enable = true;
    zsh.enable = true;
  };

  home.stateVersion = "24.05";
  home.homeDirectory = "/home/aibarchok";

  sops.defaultSopsFile = ../../../secrets/default.yaml;
  sops.age.sshKeyPaths = [ "/home/aibarchok/.ssh/id_ed25519" ];
}
