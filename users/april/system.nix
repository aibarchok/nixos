{ pkgs, ... }: {
  imports = [
    ../../hardware/april
    ../../modules/system
  ];

  networking.hostName = "april";

  environment.systemPackages = with pkgs; [
    thinkfan
  ];

  users.users.aibarchok = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" "video" ];
  };

  modules = {
    kernel.enable = true;
    networkmanager.enable = true;
    pipewire.enable = true;
    bluetooth.enable = false;
    tlp.enable = true;
    mpd.enable = true;
    radeon.enable = true;
    systemd-boot.enable = true;
    lightdm.enable = true;
    steam.enable = true;
    stylix.enable = true;
    virt.enable = true;
    suckless.enable = true;
    xorg.enable = true;
  };

  system.stateVersion = "24.05";
}

