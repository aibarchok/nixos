{ pkgs, ... }: {
  imports = [
    ../../hardware/march
    ../../modules/system
  ];

  networking.hostName = "march";

  users.users.aibarchok = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" "video" ];
  };

  programs.corectrl.enable = true;

  environment.systemPackages = with pkgs; [
    nvtopPackages.nvidia
    nvfancontrol
    mesa-demos
  ];

  modules = {
    kernel.enable = true;
    networkmanager.enable = true;
    nvidia.enable = true;
    pipewire.enable = true;
    bluetooth.enable = false;
    mpd.enable = true;
    systemd-boot.enable = true;
    lightdm.enable = true;
    stylix.enable = true;
    suckless.enable = true;
    steam.enable = true;
    xorg.enable = true;
    lact.enable = true;
    #flatpak.enable = true;
  };

  system.stateVersion = "24.05";
}
