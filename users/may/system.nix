{ pkgs, ... }: {
  imports = [
    ../../hardware/may
    ../../services
    ../../modules/system
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;

  networking.hostName = "fennec";

  users.users.may = {
    isNormalUser = true;
    description = "may because i rent my first vps in 29th may";
    extraGroups = [ "networkmanager" "wheel" ];
    openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEyZRDsxDI3Le5PFI2iCaV8WP0V8fGT7roOHFnVut1bj aibarchok@april" ];
    packages = with pkgs; [
      hugo
      caddy
      # ...
    ];
  };

  networking = {
    networkmanager.enable = true;
    firewall = {
      enable = true;
      allowedTCPPorts = [
        5379 # ssh
        443 # caddy
        80 # nginx-acme
        143 # imap
        587 # smtp
        3000 # forgejo
        # ...
      ];
    };
  };
  systemd = {
    services.NetworkManager-wait-online.enable = false;
    network.wait-online.enable = false;
  };

  # NETWORKING
  systemd.network.enable = true;
  networking.useDHCP = false;
  systemd.network.networks."10-wan" = {
    networkConfig.DHCP = "no";
    matchConfig.Name = "ens18";
    address = [ "194.59.186.11/32" ];
    routes = [
      { routeConfig = { Destination = "172.16.0.1"; }; }
      { routeConfig = { Gateway = "172.16.0.1"; GatewayOnLink = true; }; }
    ];
  };

  environment.systemPackages = with pkgs; [
    git
    # ...
  ];

  time.timeZone = "Europe/Helsinki";

  services.openssh = {
    enable = true;
    ports = [ 5379 ];
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      AllowUsers = null;
      UseDns = false;
      X11Forwarding = false;
      PermitRootLogin = "no";
    };
  };

  users.motd = "
  ⠀⠀⠀⠀⠀⠀⠀⠀⠀⡔⠠⢤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⡴⠒⠒⠒⠒⠒⠶⠦⠄⢹⣄⠀⠀⠑⠄⣀⡠⠤⠴⠒⠒⠒⠀⠀
⢇⠀⠀⠀⠀⠀⠀⠐⠋⠀⠒⠂⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠀
⠈⢆⠀⠀⠀⠀⡤⠤⣄⠀⠀⠀⠀⡤⠤⢄⠀⠀⠀⠀⠀⣠⠃⠀
⠀⡀⠑⢄⡀⡜⠀⡜⠉⡆⠀⠀⠀⡎⠙⡄⠳⡀⢀⣀⣜⠁⠀⠀
⠀⠹⣍⠑⠀⡇⠀⢣⣰⠁⠀⠀⠀⠱⣠⠃⠀⡇⠁⣠⠞⠀⠀⠀
⠀⠀⠀⡇⠔⣦⠀⠀⠀⠈⣉⣀⡀⠀⠀⠰⠶⠖⠘⢧⠀⠀⠀⠀
⠀⠀⠰⠤⠐⠤⣀⡀⠀⠈⠑⣄⡁⠀⡀⣀⠴⠒⠀⠒⠃⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠘⢯⡉⠁⠀⠀⠀⠀⠉⢆⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⢀⣞⡄⠀⠀⠀⠀⠀⠀⠈⡆⠀⠀⠀⠀⠀⠀⠀
  What the fuck did you just say to me, you little bitch?

  I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little clever comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo
						";

  modules = {
    kernel.enable = true;
    systemd-boot.enable = false;

    services = {
      caddy.enable = true;
      mailserver.enable = true;
      forgejo.enable = false;
    };
  };

  system.stateVersion = "24.05";
}
