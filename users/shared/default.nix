{ lib, pkgs, ... }:
let
  inherit (lib) mkDefault;
in
{
  nixpkgs.config.allowUnfree = mkDefault true;

  nix.settings = {
    experimental-features = mkDefault [ "nix-command" "flakes" ];
    keep-derivations = mkDefault true;
    keep-outputs = mkDefault true;
    auto-optimise-store = mkDefault true;
    sandbox = mkDefault true;
  };

  security.pam.loginLimits = [{
    domain = "*";
    type = "soft";
    item = "nofile";
    value = "1048576";
  }];

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;

  environment.systemPackages = with pkgs; [
    lsd
    fzf
    ripgrep

    unzip
    p7zip

    fastfetch

    htop

    mpd
    mpv

    alsa-utils

    wine
    winetricks
    #wineWowPackages.waylandFull
    wineWowPackages.stable

    gtk2
    gtk3
    gtk4
  ];


  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [
  { 
    users = [ "aibarchok" ];
    keepEnv = true;
    persist = true;
    #  commands = [{
    #	command = "/run/current-system/sw/bin/light";
    #	options = [ "NOPASSWD" ];
    #  }];
    }];

  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    #extraPackages32
  };

  programs.dconf.enable = true;

  time.timeZone = mkDefault "Asia/Almaty";

  i18n.defaultLocale = mkDefault "en_US.UTF-8";

  sops.defaultSopsFile = ./secrets/default.yaml;

  programs.direnv.enable = true;

  environment = {
    defaultPackages = lib.mkForce [ ];
    sessionVariables = {
      NIXPKGS_ALLOW_UNFREE = "1";
      NIXPKGS_ALLOW_INSECURE = "1";
      GTK_THEME = "Adwaita-dark";
    };
  };
}

