{ lib, ... }:
let
  inherit (lib) mkDefault;
in
{
  nixpkgs.config.allowUnfree = mkDefault true;

  nix.settings = {
    experimental-features = mkDefault [ "nix-command" "flakes" ];
    keep-derivations = mkDefault true;
    keep-outputs = mkDefault true;
    auto-optimise-store = mkDefault true;
    sandbox = mkDefault true;
  };

  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [{
    users = [ "may" ];
    keepEnv = true;
    persist = true;
  }];

  i18n.defaultLocale = mkDefault "en_US.UTF-8";

  sops.defaultSopsFile = ./secrets/default.yaml;

}

